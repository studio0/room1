﻿using UnityEngine;
using System.Collections;
using GVR.Gfx;

namespace GVR.Entity {
	public class WalkableProperty : InteractableProperty {		
		public enum MovementMode
		{
			Navigation,
			Teleportation
		}

		[Tooltip("Will the player teleport to the intended location, or navigate there")]
		public MovementMode movementMode = MovementMode.Navigation;

		//TODO Move this out
//		[Tooltip("Cursor to show at the end of the ray")]
//		public GameObject cursor;

		[Tooltip("Cursor to show on the floor")]
		public GameObject floorCursor;

		[Tooltip("Reference to the renderer of the model used for this object.")]
		private Renderer modelRenderer;

		[Tooltip("The material used to display the model normally.")]
		private Material normalMaterial;

		[Tooltip("A material used for OutlinedObjectFX highlighting.")]
		public Material HighlightMaterial;

		[Tooltip("The player agent to move.")]
		public NavMeshAgent playerAgent;

		[Tooltip("The player object to teleport.")]
		public GameObject player;

		void Start()
		{
			modelRenderer = gameObject.GetComponent <Renderer> ();
			normalMaterial = modelRenderer.material;

			//TODO: Fix the bug where it messes up subsequent pointer orientation
			playerAgent.updateRotation = false;

			floorCursor.SetActive (false);
		}

		public override bool OnRemotePointEnter() {
			base.OnRemotePointEnter();
			if (HighlightMaterial != null) {
				modelRenderer.material = HighlightMaterial;
			}

			floorCursor.SetActive (true);

			return true;
		}

		public override bool OnRemotePointExit() {
			base.OnRemotePointExit();
			modelRenderer.material = normalMaterial;

			floorCursor.SetActive (false);

			return true;
		}

		public override bool OnRemotePointMovedWithin(Transform remoteTransform) {
			base.OnRemotePointMovedWithin(remoteTransform);

			RaycastHit hitInfo;
			Vector3 rayDirection = remoteTransform.forward;
			if (Physics.Raycast (remoteTransform.position, rayDirection, out hitInfo)) {
				if (hitInfo.collider && hitInfo.collider.gameObject) {
					Vector3 markerPosition = hitInfo.point;

					floorCursor.transform.position = markerPosition;
				}
			}
			return true;
		}

		public override bool OnRemotePressDown() {
			base.OnRemotePressDown();
			Vector3 newPosition = floorCursor.transform.position;

			//Move the player
			switch (movementMode) {
				case MovementMode.Navigation:									
					playerAgent.destination = newPosition; 
					break;
				case MovementMode.Teleportation:								
					player.transform.position = newPosition; 
					break;
			}

			return true;
		}			
	}
}