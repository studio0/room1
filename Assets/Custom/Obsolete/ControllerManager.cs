﻿// Copyright 2016 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissioßns and
// limitations under the License.

using UnityEngine;
using UnityEngine.UI;

public class ControllerManager : MonoBehaviour {
	public float kCursorDistanceFromPlayer = 20;

	public GameObject controllerPivot;
	public GameObject messageCanvas;
	public Text messageText;

	public Material cubeInactiveMaterial;
	public Material cubeHoverMaterial;
	public Material cubeActiveMaterial;
	public Material floorHoverMaterial;

	public GameObject floor;
	public GameObject player;
	private NavMeshAgent playerAgent;

	public GameObject cursor;
	public GameObject floorCursor;

	private Renderer controllerCursorRenderer;

	// Currently selected GameObject.
	private GameObject selectedObject;

	public enum MovementMode
	{
		Navigation,
		Teleportation
	}

	public MovementMode movementMode = MovementMode.Navigation;

	// True if we are dragging the currently selected GameObject.
	private bool dragging;

	#if UNITY_HAS_GOOGLEVR && (UNITY_ANDROID || UNITY_EDITOR)
	void Awake() {
	}

	void Start()
	{		
		playerAgent = player.GetComponent<NavMeshAgent>();
		//Rotation messes up the GVRController.Orientation
		//TODO: Make this work
		playerAgent.updateRotation = false;
	}

	void Update() {
		UpdatePointer();
		UpdateStatusMessage();
	}

	private void UpdatePointer() {
		Quaternion pointerOrientation = GvrController.Orientation;

		if (GvrController.State != GvrConnectionState.Connected) {
			controllerPivot.SetActive(false);
		}
		controllerPivot.SetActive(true);
		controllerPivot.transform.rotation = pointerOrientation;

		//Set at some units away from player
		Vector3 cursorPosition = controllerPivot.transform.position + pointerOrientation * Vector3.forward * kCursorDistanceFromPlayer;
		cursor.transform.position = cursorPosition;

		if (dragging) {
			if (GvrController.TouchUp) {
				EndDragging();
			}
		} else {
			RaycastHit hitInfo;
			Vector3 rayDirection = pointerOrientation * Vector3.forward;
			if (Physics.Raycast(controllerPivot.transform.position, rayDirection, out hitInfo)) {
				if (hitInfo.collider && hitInfo.collider.gameObject) {
					SetSelectedObject(hitInfo.collider.gameObject);

					Vector3 markerPosition = hitInfo.point;//new Vector3 (hitInfo.point.x, cursor.transform.position.y, hitInfo.point.z);
					cursor.transform.position = markerPosition;
					floorCursor.transform.position = markerPosition;

					bool isFloor = selectedObject == floor;

					//Switch cursors
					cursor.SetActive (!isFloor);
					floorCursor.SetActive (isFloor);

					//Teleport on click
					if (isFloor) {								
						if (GvrController.ClickButtonDown) {
							Vector3 newPosition = new Vector3 (hitInfo.point.x, player.transform.position.y, hitInfo.point.z);

							//Move the player
							switch (movementMode) {
								case MovementMode.Navigation:									
									playerAgent.destination = newPosition; 
									break;
								case MovementMode.Teleportation:								
									player.transform.position = newPosition; 
									break;
							}
						}
					}
				}
			} else {
				SetSelectedObject(null);
			}

			if (GvrController.TouchDown && selectedObject != null && selectedObject != floor) {
				StartDragging();
			}
		}			
	}

	private void SetSelectedObject(GameObject obj) {
		if (null != selectedObject) {
			selectedObject.GetComponent<Renderer>().material = cubeInactiveMaterial;
		}
		if (null != obj) {			
			obj.GetComponent<Renderer>().material = obj == floor ? floorHoverMaterial : cubeHoverMaterial;
		}
		selectedObject = obj;
	}

	private void StartDragging() {
		dragging = true;
		selectedObject.GetComponent<Renderer>().material = cubeActiveMaterial;

		// Reparent the active cube so it's part of the ControllerPivot object. That will
		// make it move with the controller.
//		selectedObject.transform.SetParent(controllerPivot.transform, true);
	}

	private void EndDragging() {
		dragging = false;
		selectedObject.GetComponent<Renderer>().material = cubeHoverMaterial;

		// Stop dragging the cube along.
		selectedObject.transform.SetParent(null, true);
	}

	private void UpdateStatusMessage() {
		// This is an example of how to process the controller's state to display a status message.
		switch (GvrController.State) {
		case GvrConnectionState.Connected:
			messageCanvas.SetActive(false);
			break;
		case GvrConnectionState.Disconnected:
			messageText.text = "Controller disconnected.";
			messageText.color = Color.white;
			messageCanvas.SetActive(true);
			break;
		case GvrConnectionState.Scanning:
			messageText.text = "Controller scanning...";
			messageText.color = Color.cyan;
			messageCanvas.SetActive(true);
			break;
		case GvrConnectionState.Connecting:
			messageText.text = "Controller connecting...";
			messageText.color = Color.yellow;
			messageCanvas.SetActive(true);
			break;
		case GvrConnectionState.Error:
			messageText.text = "ERROR: " + GvrController.ErrorDetails;
			messageText.color = Color.red;
			messageCanvas.SetActive(true);
			break;
		default:
			// Shouldn't happen.
			Debug.LogError("Invalid controller state: " + GvrController.State);
			break;
		}
	}
	#endif  // UNITY_HAS_GOOGLEVR && (UNITY_ANDROID || UNITY_EDITOR)
}
