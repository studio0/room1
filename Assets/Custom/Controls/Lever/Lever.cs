﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

/// <summary>
/// VR lever. Acts like a UI slider but is moved physically like a lever.
/// </summary>
public class Lever : MonoBehaviour {

	/// <summary>
	/// Callbacks.
	/// </summary>

//	[Tooltip("Minimum angle in degrees, get's updated by and updates the joint if there is one")]
	public float onAngle = 60;
//
//	[Tooltip("Maximum angle in degrees, get's updated by and updates the joint if there is one")]
	public float offAngle = -60;


	/// <summary>
	/// The current hinge.
	/// </summary>
	private HingeJoint joint;

	public bool isInitiallyOn = true;

	public bool isOn {
		get {
			float angleHinge = joint.angle;
			float distanceFromOn = Mathf.Abs(angleHinge - onAngle);
			float distanceFromOff = Mathf.Abs(angleHinge - offAngle);

			return distanceFromOn < distanceFromOff;
		}
		set {
			//Spring to closest number
			JointSpring spring = joint.spring;
			spring.spring = 20;
			spring.damper = 3;
			spring.targetPosition = value ? onAngle : offAngle;
			joint.spring = spring;
			joint.useSpring = true;
		}
	}

	void Start()
	{
		joint = gameObject.GetComponent<HingeJoint>();
		isOn = isInitiallyOn;
	}

	void Update()
	{
		//Get closest number and snap to it
		bool localIsOn = isOn;	

		if (joint.velocity == 0) {
			isOn = localIsOn;
		}		
	}
}
