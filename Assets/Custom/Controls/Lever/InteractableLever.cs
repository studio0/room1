﻿using UnityEngine;
using System.Collections;

namespace GVR.Entity
{
	public class InteractableLever : InteractableProperty {

		public Lever lever;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override bool OnRemotePointEnter()
		{
			base.OnRemotePointEnter ();
			return true;
		}

		public override bool OnRemotePointExit()
		{
			base.OnRemotePointExit ();
			return true;
		}

		public override bool OnRemotePressDown() {
			base.OnRemotePressDown ();

			lever.isOn = !lever.isOn;

			return true;
		}
	}
}
